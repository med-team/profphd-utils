PACKAGE := profphd-utils
VERSION := 1.0.10
DISTDIR := $(PACKAGE)-$(VERSION)

mandir := $(prefix)/share/man
man1dir := $(mandir)/man1

ARCH		= LINUX
F77       	= gfortran
AM_FFLAGS       := -O2 -fbounds-check -Wuninitialized -fbacktrace -g
#AM_FFLAGS       := $(AM_FFLAGS) -Wall -Wno-unused -Wtabs

BINARIES=convert_seq filter_hssp

MAN1 := convert_seq.1 filter_hssp.1

MANS := $(MAN1)

all: $(BINARIES) $(MANS)

%.1 : %.pod Makefile
	pod2man -c 'User Commands' -r "$(VERSION)" -name $(shell echo "$(basename $@)" | tr '[:lower:]' '[:upper:]') "$<" "$@"

convert_seq filter_hssp : maxhom.common maxhom.param

convert_seq : convert_seq.f lib-maxhom.f lib-sys-$(ARCH).f
	$(F77) $(CPPFLAGS) $(AM_FFLAGS) $(FFLAGS) $(LDFLAGS) -o $@ convert_seq.f lib-maxhom.f lib-sys-$(ARCH).f

filter_hssp : filter_hssp.f lib-maxhom.f lib-sys-$(ARCH).f
	$(F77) $(CPPFLAGS) $(AM_FFLAGS) $(FFLAGS) $(LDFLAGS) -o $@ filter_hssp.f lib-maxhom.f lib-sys-$(ARCH).f

#lib-maxhom.o : maxhom.common maxhom.param

install:
	mkdir -p $(DESTDIR)$(prefix)/bin && \
		cp $(BINARIES) $(DESTDIR)$(prefix)/bin/
	mkdir -p $(DESTDIR)$(man1dir) && \
		cp $(MAN1) $(DESTDIR)$(man1dir)/

clean:
	rm -f *.o convert_seq filter_hssp convert_seq.1 filter_hssp.1

dist:	$(DISTDIR)
	tar -c -f - "$(DISTDIR)" | gzip -c >$(DISTDIR).tar.gz
	rm -rf $(DISTDIR)

$(DISTDIR): distclean
	rm -rf $(DISTDIR) && mkdir -p $(DISTDIR) && \
	rsync -avC \
		--exclude /*-stamp \
		--exclude .*.swp \
		AUTHORS \
		ChangeLog \
		compile_macintel.csh \
		compile.pl \
		convert_seq.f \
		convert_seq.pod \
		COPYING \
		dead.f \
		filter_hssp.f \
		filter_hssp.pod \
		lib-convert.f \
		lib-maxhom.f \
		lib-maxhom-node-pvm3.f \
		lib-metr.f \
		lib-metr-sys.f \
		lib-sys-LINUX.f \
		long.msf \
		Makefile \
		maxhom.common \
		maxhom.default \
		maxhom.f \
		maxhom.param \
		metr2st_make.f \
		$(PACKAGE).spec \
		ReadMe \
		ReadMe-linux \
		ReadMe-mac \
		$(DISTDIR)/;

distclean: clean
	rm -rf\
		$(DISTDIR) \
		$(DISTDIR).tar.gz

help:
	@echo "all*: convert_seq filter_hssp"
	@echo "convert_seq"
	@echo "filter_hssp"
	@echo "install"
	@echo
	@echo "VARIABLES"
	@echo "prefix - prefix all paths with 'prefix'"
	@echo "DESTDIR - installation directory prefix"

.PHONY: all clean dist distclean help install
