Summary: profphd helper utilities
Name: profphd-utils
Version: 1.0.8
Release: 1
License: GPL
Group: Applications/Science
Source: ftp://rostlab.org/%{name}/%{name}-%{version}.tar.gz
URL: http://rostlab.org/
BuildRoot: %{_tmppath}/%{name}-%{version}-root
BuildRequires: gcc-gfortran

%description
 The package provides the following binary utilities: convert_seq, filter_hssp

%prep
%setup -q

%build
make DESTDIR=%{buildroot} prefix=/usr AM_FFLAGS="-O2 -fbounds-check -Wuninitialized -Wall -Wno-unused"

%install
rm -rf $RPM_BUILD_ROOT
make DESTDIR=%{buildroot} prefix=/usr install

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
%doc AUTHORS
%doc COPYING
%doc ReadMe
%doc ReadMe-linux
%{_mandir}/*/*
%{_bindir}/*

%changelog
* Fri Jan 13 2012 Laszlo Kajan <lkajan@rostlab.org> - 1.0.8-1
- new upstream
* Tue Jun 21 2011 Laszlo Kajan <lkajan@rostlab.org> - 1.0.6-2
- spec now in dist root
* Sat Jun 18 2011 Laszlo Kajan <lkajan@rostlab.org> - 1.0.6-1
- First rpm package
